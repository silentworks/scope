<?php
/**
 * @package: Scope
 * @subpackage: build
 */
function getSnippetContent($filename){
    $o = file_get_contents($filename);
    $o = trim(str_replace(array('<?php','?>'),'',$o));
    return $o;
}


/**
 * Parse readme template and return content
 *
 * @return string
 */
function updateReadme(){
    global $modx;

    // Load Smarty Instance
    $modx->getService('smarty','smarty.modSmarty');

    /** @var string $tpl Smarty Template */
    $tpl = dirname(__FILE__).'/readme.tpl';

    /** @var array $params Template data */
    $params = array(
        'date' => date('Y-m-d h:i:s e'),
        'year' => date('Y'),
        'license' => '[NOT SURE]',
        'commit' => gitCommitId(dirname(dirname(dirname(__FILE__)))),
        'version' => PKG_VERSION.' '.PKG_RELEASE
    );

    /** @var string $rendered Rendered template  */
    $rendered = $modx->smarty->fetch($tpl,$params);
    return $rendered;
}


/**
 * Get the last git commit id from a repo at $repoRoot
 *
 * @param string   $repoRoot   Path to root of git repo
 * @return string
 */
function gitCommitId($repoRoot){
    // Check for a .git/ORIG_HEAD file
    $ORIG_HEAD = $repoRoot.'/.git/ORIG_HEAD';
    if(!is_readable($ORIG_HEAD)){ return ''; };

    $commitId = file_get_contents($ORIG_HEAD);
    return trim($commitId);
}