<?php
/**
 * Scope
 *
 * Loads system settings into build
 *
 * @package Scope
 * @subpackage build
 */
$s = array(
    'scope' => array(
        'currentTheme' => 'scope',
        // 'viewParser' => '\\Scope\\Extras\\View\\Twig'
    )
);

$settings = array();
foreach ($s as $area => $sets) {
    foreach ($sets as $key => $value) {
        if (is_bool($value)) { $type = 'combo-boolean'; }
        else { $type = 'textfield'; }
        $settings[$area.'.'.$key] = $modx->newObject('modSystemSetting');
        $settings[$area.'.'.$key]->fromArray(array(
            'key' => $area.'.'.$key,
            'name'=> $area.'.set_'.$key,
            'value' => $value,
            'xtype' => $type,
            'namespace' => $area,
            'area' => $area
        ), '', true, true);
    }
}

return $settings;
