<?php
/**
 * Scope
 *
 * Add snippets to build
 *
 * @package Scope
 * @subpackage build
 */

$snips = array(
    'scope' => 'Scope helper snippet that generates current theme paths.',
);

$snippets = array();
$i = 0;

foreach($snips as $sn => $sdesc) {
    $i++;
    $sfilename = strtolower($sn);
    $file = $sources['snippets'] . $sfilename . '.snippet.php';
    $snippets[$i]= $modx->newObject('modSnippet');
    $snippets[$i]->fromArray(array(
        'name' => $sn,
        'description' => $sdesc,
        'source' => 1,
        'static' => 1,
        'static_file' => str_replace(MODX_ROOT, '', $file)
    ), '', true, true);

    $property = $sources['build'] . 'properties/properties.' . $sfilename . '.php';
    if (file_exists($property)) {
        $properties = include $property;
        $snippets[$i]->setProperties($properties);
        unset($properties);
    }
}

return $snippets;
