# Scope

MODX template/snippets/chunks management system.

## Simple Setup

Create the following system settings in order to use this exta.

`scope.assets_path`: `path/to/scope/assets/directory`

`scope.core_path`: `path/to/scope/core/directory`

`scope.currentTheme`: `theme_directory_name`

You will need to create a snippet and map to the snippet in Scope core directory, do the same for the plugin and set the event. This is setup as a plugin
temporarily until a UI comes into place.

Manually create a directory in your MODX install assets directory called `themes`, this is where you will store all Scope themes.

## View Parser Setup

This is a bit more advanced coming soon.

