<?php
$corePath = $modx->getOption('scope.core_path', null, $modx->getOption('core_path') . 'components/scope/');
require_once $corePath . 'core/core.php';

$case = $modx->getOption('case', $scriptProperties, '');
$toPlaceholder = $modx->getOption('toPlaceholder', $scriptProperties, false);

$scope = new \Scope\Core\Core($modx);

switch ($case) {
    case 'themePathUrl':
    	$plc = 'themePathUrl';
        $themePath = \Scope\Core\Core::currentThemePath();
    break;
    default:
    	$plc = 'themePath';
        $themePath = \Scope\Core\Core::themePath();
    break;
}

if ($toPlaceholder) {
	$modx->setPlaceholder($plc, $themePath);
} else {
	return $themePath;
}