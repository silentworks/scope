<?php
$corePath = $modx->getOption('scope.core_path', null, $modx->getOption('core_path') . 'components/scope/');
require_once $corePath . 'core/core.php';
require_once $corePath . 'core/manifest.php';

switch($modx->event->name) {
	/*case 'OnManagerPageInit':
		$cssFile = $modx->getOption('scope.assets_url',null,$modx->getOption('assets_url').'components/scope/').'css/mgr/scope.css';
		$modx->regClientCSS($cssFile);
	break;*/
	// case 'OnWebPageInit':
	// case 'OnWebPagePrerender':
	case 'OnHandleRequest':
		if ($modx->context->get('key') !== 'mgr') {
            $manifest = new \Scope\Core\Manifest();
			$scope = new \Scope\Core\Core($modx, $manifest);
			$scope->scanThemeDirectory();
			$scope->checkScopeFileMap();
		}
	break;
}
