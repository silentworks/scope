<?php
/**
 * @package modx
 * @subpackage mysql
 */
$xpdo_meta_map['ScopeResource']= array (
  'package' => 'modx',
  'version' => '1.1',
  'extends' => 'modResource',
  'fields' =>
  array (
  ),
  'fieldMeta' =>
  array (
  ),
  'aggregates' =>
  array (
    'Template' =>
    array (
      'class' => 'ScopeTemplate',
      'local' => 'template',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
