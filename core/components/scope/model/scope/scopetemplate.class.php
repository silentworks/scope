<?php
/**
 * ScopeTemplate CRC
 *
 * @package: Scope
 * @subpackage: build
 */

$scopeBasePath = dirname(dirname(dirname(__FILE__)));
require_once MODX_CORE_PATH . 'model/modx/modtemplate.class.php';
require_once $scopeBasePath . '/core/core.php';

\Scope\Core\Core::registerAutoloader();

/**
 * Including Composer Autoloader
 */
$file = $scopeBasePath . '/vendor/autoload.php';
if (file_exists($file)) {
    require $file;
}

class ScopeTemplate extends modTemplate {
    public $_static_template = false;
    public $static;
    private $view;

    /**
     * @param xPDO $xpdo A reference to the xPDO|modX instance
     */
    public function __construct(xPDO & $xpdo)
    {
        parent::__construct($xpdo);
        $this->view($this->getOption('scope.viewParser', null, '\\Scope\\Core\\Simple'));

        // Set cacheable flag based on twig-or-not
        $shouldCache = get_class($this->view) === 'Scope\Core\Simple';
        $this->setCacheable($shouldCache);
        $this->_static_template = !$shouldCache;
        $this->static = !$shouldCache;
    }

    public function process($properties= null, $content= null)
    {
        if ($this->static != 1) {
            return parent::process($properties, $content);
        }
        ob_start();
            include $this->getSourceFile();
            $staticFileContent = ob_get_contents();
        ob_end_clean();

        $this->_output = $this->view->render($staticFileContent);

        return $this->_output;
    }

    public function view($viewClass = null)
    {
        if (! is_null($viewClass)) {

            if (! class_exists($viewClass)) {
                $msg = 'Scope is missing view parser %s called in %s on line: %s in file: %s';
                throw new Exception(sprintf($msg, $viewClass, __METHOD__, __LINE__, basename(__FILE__)));
            }


            if ($viewClass instanceOf \Scope\View) {
                $this->view = $viewClass;
            } else {
                $this->view = new $viewClass();
            }

            $pageData = $this->xpdo->resource->toArray();

            // Grab tvs into the mix
            $tvObjs = $this->xpdo->resource->getTemplateVars();
            foreach($tvObjs as $tvObj) {
                $pageData['tv'][$tvObj->get('name')] = $this->processTV($tvObj);
            }

            $data = array('page'=>$pageData);

            $data = $this->xpdo->resource->getTemplateData($data);

            /** @var \Scope\Core\Simple $this->view */
            $this->view->appendData(array_merge($data, array(
                'site_name' => $this->xpdo->config['site_name'],
                'site_url' => $this->xpdo->config['site_url']
            )));
            $this->view->setThemesDirectory($this->getOption('scope.themeDir', null, $this->getOption('assets_path') . 'themes/'));
            $this->view->setTheme($this->getOption('scope.currentTheme'));
            $this->view->setDatasource($this->getOption('scope.dataSource', null, 'Xpdo'));
            $this->view->setConfig(array(
                'currentContext' => $this->xpdo->context->get('key'),
                'assetsPath' => $this->getOption('assets_path'),
                'corePath' => $this->getOption('core_path'),
                'cachePath' => $this->getOption('cache_path'),
                'debug' => $this->getOption('scope.debug', null, true),
                'currentThemePath' => \Scope\Core\Core::currentThemePath(true),
                'modx' => $this->xpdo
            ));
            //$this->view->setPlugins();

        }

        return $this->view;
    }

    public function processTV(modTemplateVar $tv)
    {
        $tvName = $tv->get('name');
        $val = $this->xpdo->resource->getTVValue($tv->get('name'));
        $tvArray = $tv->toArray();

        if (isset($tvArray['output_properties']['delimiter'])) {
            // Split at delimiter
            $val = explode($tvArray['output_properties']['delimiter'], $val);
        }

        return $val;
    }
}
