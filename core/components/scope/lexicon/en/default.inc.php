<?php
/**
 * Default English Lexicon Entries
 *
 * @subpackage lexicon
 */
$_lang['scope'] = 'Themes';
$_lang['scope.desc'] = 'Theming framework that allows easy interchange between themes';

$_lang['scope.resource.create'] = 'Scope Resource';
$_lang['scope.resource.create_here'] = 'Create Scope Resource';
$_lang['scope.resource'] = 'Scope Resource';

/* Settings lexicons */
$_lang['scope.set_currentTheme'] = 'Current Theme Slug';
$_lang['scope.set_viewParser'] = 'View Parser';
