<?php
require_once dirname(dirname(__FILE__)) . '/core/core.php';
require_once dirname(dirname(__FILE__)) . '/model/scope.class.php';

\Scope\Core\Core::registerAutoloader();

use \Scope\Core\Core;
use \Scope\Core\Manifest;

/**
 * Scope
 *
 * Loads the home page.
 *
 * @package Scope
 * @subpackage controllers
 */
class ScopeWorkspaceManagerController extends ScopeBaseManagerController
{

    public function process(array $scriptProperties = array())
    {
        $manifest = new Manifest();
        $scope = new Core($this->modx, $manifest);
        $scope->scanThemeDirectory();
        $scope->checkScopeFileMap();

        $themes = $scope->getThemes(true);

        /* Scope Class */
        $scpClass = new Scope($this->modx);

        $this->addHtml(
        '<script type="text/javascript">
			window.config = ' . $this->modx->toJSON($scpClass->config) . ';
			window.config.currentTheme = "' . Core::getCurrentTheme() . '";
            window.config.scopeThemes = ' . json_encode($themes) . ';
            window.config.siteId = "' . $this->modx->user->getUserToken($this->modx->context->get('key')) . '";
        </script>'
        );
    }

    public function getPageTitle()
    {
        return $this->modx->lexicon('scope');
    }

    public function loadCustomCssJs()
    {
        $this->modx->regClientStartupScript($this->scope->config['jsUrl'] . 'mgr/main.js');
    }

    public function getTemplateFile()
    {
        return $this->scope->config['templatesPath'] . 'workspace.tpl';
    }
}
