<?php

class ScopeResourceCreateManagerController extends ResourceCreateManagerController {
    /**
     * Returns language topics
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('resource', 'scope:default');
    }
}
