<?php
require_once dirname(dirname(dirname(__FILE__))) . '/core/core.php';

\Scope\Core\Core::registerAutoloader();

class ScopeGetListProcessor extends modObjectProcessor {
    /* Class in model directory */
    public $classKey = '';

    /* Language package to load */
    public $languageTopics = array('scope:default');

    public function process() {
        $manifest = new \Scope\Core\Manifest();
        $scope = new \Scope\Core\Core($this->modx, $manifest);
        $scope->scanThemeDirectory();
        $scope->checkScopeFileMap();

        return $this->outputArray($scope->getThemes(true));
    }
}

return 'ScopeGetListProcessor';
