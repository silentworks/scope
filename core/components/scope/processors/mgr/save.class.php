<?php
require_once dirname(dirname(dirname(__FILE__))) . '/core/core.php';

\Scope\Core\Core::registerAutoloader();

use \Scope\Core\Core;
use \Scope\Core\Manifest;

class ScopeSaveProcessor extends modObjectProcessor
{
    /* Class in model directory */
    public $classKey = '';

    /* Language package to load */
    public $languageTopics = array('scope:default');

    public function process()
    {
        $canSave = true;
        $force = $this->getProperty('force', false);
        $newTheme = $this->getProperty('slug');
        $getTheme = $this->modx->getObject('modSystemSetting', 'scope.currentTheme');
        $currentTheme = $getTheme->get('value');

        if ($newTheme !== $currentTheme) {
            $diff = $this->checkScopeDiff($newTheme, $currentTheme);

            if ($diff || $force) {
                $getTheme->set('value', $newTheme);
                $getTheme->save();

                /* Refresh System Settings Cache */
                $this->modx->cacheManager->refresh(
                    array(
                        'system_settings' => array()
                    )
                );
            } else {
                $canSave = false;
            }
        }

        if ($canSave) {
            return $this->success(
                'You have successfully changed your theme.',
                array(
                    'currentTheme' => $getTheme->get('value')
                )
            );
        } else {
            return $this->failure('Difference in data structure', $diff);
        }
    }

    private function checkScopeDiff($newTheme, $currentTheme)
    {
        $manifest = new Manifest();
        $scope = new Core($this->modx, $manifest);
        $scope->scanThemeDirectory();
        $newTheme = $scope->getTheme($newTheme);
        $currentThemeArray = $scope->getTheme($currentTheme);

        if (isset($newTheme->scopeMap) && isset($currentThemeArray->scopeMap)) {
            $nThemeScope = json_decode(json_encode($newTheme->scopeMap), true);
            $cThemeScope = json_decode(json_encode($currentThemeArray->scopeMap), true);
            $diff = $this->array_diff_assoc_recursive($nThemeScope, $cThemeScope);
            return $diff;
        }
        return true;
    }

    /**
     * Taken from php.net
     * http://www.php.net/manual/en/function.array-diff-assoc.php#73972
     * @param $array1
     * @param $array2
     * @return int
     */
    private function array_diff_assoc_recursive($array1, $array2)
    {
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key])) {
                    $difference[$key] = $value;
                } elseif (!is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = $this->array_diff_assoc_recursive($value, $array2[$key]);
                    if ($new_diff != false) {
                        $difference[$key] = $new_diff;
                    }
                }
            } elseif (!isset($array2[$key]) || $array2[$key] != $value) {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? 0 : $difference;
    }
}

return 'ScopeSaveProcessor';
