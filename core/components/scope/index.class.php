<?php
/**
 * Scope
 */
require_once dirname(__FILE__) . '/model/scope.class.php';
/**
 * @package Scope
 */
class IndexManagerController extends modExtraManagerController {
    public static function getDefaultController() { return 'workspace'; }
}

abstract class ScopeBaseManagerController extends modManagerController {
    /** @var Scope $scope */
    public $scope;

    public function initialize() {
        $this->scope = new Scope($this->modx);

        /* Add AngularJS and Alertify */
        $this->modx->regClientStartupScript($this->scope->config['jsUrl'] . 'mgr/angular.js');
        $this->modx->regClientStartupScript($this->scope->config['jsUrl'] . 'mgr/alertify.js');

        /* Add CSS for look and feel */
        $this->addCss($this->scope->config['cssUrl'] . 'mgr/alertify/alertify.core.css');
        $this->addCss($this->scope->config['cssUrl'] . 'mgr/alertify/alertify.default.css');
        $this->addCss($this->scope->config['cssUrl'] . 'mgr/scope.css');
        $this->addHtml('<script type="text/javascript">
        Ext.onReady(function() {
            // Ext.getCmp("modx-layout").hideLeftbar();
        });
        </script>');

        return parent::initialize();
    }

    public function getLanguageTopics() {
        return array('scope:default');
    }

    public function checkPermissions() { return true; }
}
