<?php
/**
 * Scope Core
 */
namespace Scope\Core;

//require dirname(__FILE__).'/view.php';
//require dirname(__FILE__).'/simple.php';

class Core
{
    protected $modx;
    protected $manifest;

    private $themePath;
    private static $themeUrl;
    private static $currentTheme;
    private static $themeRelativePath;

    protected $modClasses = array(
        'chunks' => 'modChunk',
        'category' => 'modCategory',
        'snippets' => 'modSnippet',
        'templates' => 'modTemplate',
    );

    public static function autoload($className)
    {

        $thisClass = str_replace(__NAMESPACE__.'\\', '', __CLASS__);

        $baseDir = dirname(dirname(dirname(__FILE__))) . '\\';

        if (substr($baseDir, -strlen($thisClass)) === $thisClass) {
            $baseDir = substr($baseDir, 0, -strlen($thisClass));
        }

        $className = ltrim($className, '\\');
        $fileName  = $baseDir;
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  .= str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        $fileName = str_replace('\\',DIRECTORY_SEPARATOR,$fileName);

        $fileName = strtolower($fileName);
        if (is_readable($fileName)) {
            require $fileName;
        }
    }

    public static function registerAutoloader()
    {
        spl_autoload_register(__NAMESPACE__ . "\\Core::autoload");
    }

    public function __construct(\modX $modx, Manifest $manifest = null)
    {
        $this->modx = $modx;
        $this->manifest = $manifest;
        $themeFullPath = $this->modx->getOption('scope.themeDir', null, $this->modx->getOption('assets_path') . 'themes/');
        $themeFullUrl = $this->modx->getOption('scope.themeUrl', null, $this->modx->getOption('assets_url') . 'themes/');
        static::$currentTheme = $this->modx->getOption('scope.currentTheme', null, 'scope');

        $modxBasePath = $this->modx->getOption('base_path');
        $themePath = str_replace($modxBasePath, '', $themeFullPath);

        $this->themePath = $themeFullPath;
        static::$themeUrl = rtrim($themeFullUrl, "/");
        static::$themeRelativePath = rtrim($themePath, "/");
        $this->debug = false;
    }

    public function scanThemeDirectory()
    {
        $themes = glob($this->themePath . '*', GLOB_ONLYDIR);
        foreach ($themes as $theme_path)
        {
            $this->getThemeConfig(basename(realpath($theme_path)));
        }

        ksort($this->themes);
        ksort($this->themesAll);

        return $this;
    }

    public static function themePath($full = false)
    {
        return $full ? static::$themeUrl : static::$themeRelativePath;
    }
    public static function currentThemePath($full = false)
    {
        return ($full ? static::$themeUrl : static::$themeRelativePath) . '/' . static::$currentTheme;
    }

    public static function getCurrentTheme()
    {
        return static::$currentTheme;
    }

    public function getTheme($slug = null)
    {
        return isset($this->themes[$slug]) ? $this->themes[$slug] : false;
    }

    public function getThemes($simple = false)
    {
        return $simple ? $this->themesAll : $this->themes;
    }

    public function checkScopeFileMap($slug = null)
    {
        foreach ($this->getThemes() as $key => $theme) {

            if (isset($theme->scope->enabled)) {
                // Check if theme exists
                $c = $this->modx->newQuery($this->modClasses['category']);
                $c->where(array(
                    'category' => $theme->scope->name
                ));
                $themeCategoryCount = $this->modx->getCount($this->modClasses['category'], $c);

                if ($themeCategoryCount < 1) {
                    $themeCategory = $this->modx->newObject($this->modClasses['category']);
                    $themeCategory->set('category', $theme->scope->name);
                    $themeCategory->save();
                } else {
                    $themeCategory = $this->modx->getObject($this->modClasses['category'], $c);
                }

                if (! empty($theme->scopeMap)) {
                    foreach($theme->scopeMap as $type => $val) {
                        switch ($type) {
                            case 'templates':
                                foreach($val as $v) {
                                    $scopedName = sprintf('%s.%s', strtolower($themeCategory->get('category')), $v->name);

                                    $t = $this->modx->newQuery($this->modClasses['templates']);
                                    $t->where(array(
                                        'category' => $themeCategory->get('id'),
                                        'templatename' => $scopedName
                                    ));

                                    if ($this->debug) {
                                        $t->construct();
                                        print_r($t->toSql()); die();
                                    }
                                    $templateCount = $this->modx->getCount($this->modClasses['templates'], $t);

                                    if ($templateCount < 1) {
                                        $template = $this->modx->newObject($this->modClasses['templates']);
                                        $template->fromArray(array(
                                            'templatename' => $scopedName,
                                            'static_file' => $this->parseString($v->file, array('{{type}}'), array($type)),
                                            'source' => 1,
                                            'static' => 1,
                                        ));
                                        $template->addOne($themeCategory);
                                        $template->save();
                                    }
                                }
                            break;

                            case 'chunks':
                            case 'snippets':
                                foreach($val as $v) {
                                    $scopedName = sprintf('%s.%s', strtolower($themeCategory->get('category')), $v->name);

                                    $t = $this->modx->newQuery($this->modClasses[$type]);
                                    $t->where(array(
                                        'category' => $themeCategory->get('id'),
                                        'name' => $scopedName
                                    ));

                                    if ($this->debug) {
                                        $t->construct();
                                        print_r($t->toSql()); die();
                                    }
                                    $chunkCount = $this->modx->getCount($this->modClasses[$type], $t);

                                    if ($chunkCount < 1) {
                                        $chunk = $this->modx->newObject($this->modClasses[$type]);
                                        $chunk->fromArray(array(
                                            'name' => $scopedName,
                                            'static_file' => $this->parseString($v->file, array('{{type}}'), array($type)),
                                            'source' => 1,
                                            'static' => 1,
                                        ));
                                        $chunk->addOne($themeCategory);
                                        $chunk->save();
                                    }
                                }
                            break;

                            case 'plugins':
                                foreach($val as $v) {
                                    $t = $this->modx->newQuery($this->modClasses['snippets']);
                                    $t->where(array(
                                        'category' => $themeCategory->get('id'),
                                        'name' => $v->name
                                    ));

                                    if ($this->debug) {
                                        $t->construct();
                                        print_r($t->toSql()); die();
                                    }
                                    $chunkCount = $this->modx->getCount($this->modClasses['snippets'], $t);

                                    if ($chunkCount < 1) {
                                        $chunk = $this->modx->newObject($this->modClasses['snippets']);
                                        $chunk->fromArray(array(
                                            'name' => sprintf('[%s] %s', $themeCategory->get('category'), $v->name),
                                            'static_file' => $this->parseString($v->file, array('{{type}}'), array($type)),
                                            'source' => 1,
                                            'static' => 1,
                                        ));
                                        $chunk->addOne($themeCategory);
                                        $chunk->save();
                                    }
                                }
                            break;

                            default:
                                # code...
                            break;
                        }
                    }
                }
            }
        }
    }

    private function getThemeConfig($slug)
    {
        if (! empty($this->themes[$slug])) {
            return $this->themes[$slug];
        }

        $path = $this->themePath . $slug;
        $manifestFullPath = $path . '/info.json';
        $scopeFullPath = $path . '/scope.json';
        if (is_dir($path) && is_file($manifestFullPath)) {
            $theme = new \stdClass();
            $theme->slug = $slug;
            $details = $this->manifest->convert($manifestFullPath);
            $this->manifest->validate($details);

            foreach ($details as $key => $value) {
                $theme->{$key} = $value;
            }

            if ($theme->scope->enabled) {
                $scopeMap = $this->manifest->convert($scopeFullPath);
                $this->manifest->validate($scopeMap);

                $theme->scopeMap = $scopeMap;
            }

            $screenShotName = isset($theme->screenshot) ? $theme->screenshot : null;

            $theme->screenshot = file_exists($this->screenshot($slug, $screenShotName)) ? $this->screenshot($slug, $screenShotName, true) : null;
            $this->themes[$slug] = $theme;
            $this->themesAll[] = $theme;

            return $theme;
        }

        return false;
    }

    private function screenshot($slug, $fileName = null, $relative = false)
    {
        if (empty($fileName)) {
            $fileName = 'screenshot.png';
        }

        $path = $relative ? static::themePath(true) : $this->themePath;

        return sprintf('%s/%s/%s', $path, $slug, $fileName);
    }




    /**
     * Generate the cache_key for a resource based on the scope and theme of its template
     *
     * @param \scopeResource $res
     * @return string
     */
    public function getResourceCacheKey(\scopeResource $res)
    {
        $ctx = $res->get('context_key');
        $id = $res->get('id');
        $currentTheme = $this->getCurrentTheme();
        $scope = $this->getTheme($currentTheme)->scope->name;
        $theme = $currentTheme;

        $cacheKey = "../scopes/{$scope}/{$ctx}/{$theme}/{$id}";
        return $cacheKey;

    }

    /**
     * Parsing Strings with search and replace values
     *
     * @param null $val
     * @param array $searchFor
     * @param array $replaceWith
     * @return mixed
     */
    private function parseString($val = null, array $searchFor = array(), array $replaceWith = array())
    {
        $search = array_merge(
            array(
                '{{themePath}}',
                '{{currentTheme}}',
                '{{currentThemePath}}'
            ),
            $searchFor
        );

        $replace = array_merge(
            array(
                static::$themeRelativePath,
                '[[++scope.currentTheme]]',
                static::$themeRelativePath . '/[[++scope.currentTheme]]'
            ),
            $replaceWith
        );

        return str_replace($search, $replace, $val);
    }
}
