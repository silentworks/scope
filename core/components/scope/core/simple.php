<?php
/**
 * Scope View
 */
namespace Scope\Core;

class Simple implements \Scope\Core\View
{
	protected $config = array();
	protected $data = array();
	protected $plugins = array();
	protected $theme;
	protected $themesDirectory;

	public function __construct()
	{

	}

	public function appendData(array $data)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Cannot append view data. Expected array argument.');
        }
        $this->data = array_merge($this->data, $data);
    }

	public function getConfig($name = null)
	{
		return !empty($name) && isset($this->config[$name]) ? $this->config[$name] : $this->config;
	}

	public function setConfig(array $config)
    {
    	$this->config = array_merge($this->config, $config);
    }

	public function setDataSource($dataSource)
	{
		$this->config['datasource'] = $dataSource;
	}

	public function getTheme()
	{
		return $this->theme;
	}

	public function setTheme($theme)
	{
		$this->theme = $theme;
	}

	public function getPlugins()
	{
		return $this->plugins;
	}

	public function setPlugins(array $plugins = array())
	{
		$this->plugins = $plugins;
	}

	public function getThemesDirectory()
	{
		return $this->themesDirectory;
	}

	public function setThemesDirectory($themesDirectory)
	{
		$this->themesDirectory = rtrim($themesDirectory, '/');
	}

	public function render($fileContent)
	{
		return $fileContent;
	}
}
