<?php
/**
 * Manifest
 */
namespace Scope\Core;

class Manifest
{
    /**
     * @var path to themes
     */
    public $themePath;

    /**
     * @var array
     */
    private $errors = array();

    public function validate(\stdClass $manifest)
    {
        if (! isset($manifest->name) || ! $manifest->name) {
            array_push($this->errors, 'Missing required field: name.');
        } else if (! is_string($manifest->name)) {
            array_push($this->errors, 'Incorrect data type for name; must be a string.');
        } else if (! preg_match('/^[a-zA-Z0-9_\.\- ]+$/', $manifest->name )) {
            array_push($this->errors, 'Name contains invalid characters.');
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return count($this->errors) > 0 ? true : false;
    }

    public function convert($manifestPath)
    {
        $json = file_get_contents($manifestPath);
        return ! empty($json) ? json_decode($json) : false;
    }
}
