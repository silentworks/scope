<?php
/**
 * Scope View
 */
namespace Scope\Core;

interface View
{
    public function appendData(array $data);

	public function render($fileContent);
}
