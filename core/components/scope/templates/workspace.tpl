<div id="scope-workspace" ng-app="Theme.App">
	<div ng-controller="AppController">
		<div id="header">
			<h2>{$_pagetitle}</h2>

			<div id="toolbar">
	        	<div id="search"><label for="s">Search:</label> <input id="s" type="text" ng-model="searchText"></div>
	        </div>
		</div>

		<div id="content">
			<ul>
				{literal}
				<li ng-repeat="theme in themes | filter:searchText" ng-class="{active: currentTheme == theme.slug}">
					<div id="{{ theme.slug }}" class="theme">
						<div class="top">
							<div class="wrapper">
								<div class="screenshot">
									<img ng-src="{{ theme.screenshot }}" alt="{{ theme.name }}" ng-show="theme.screenshot">
								</div>
								<div class="details">
									<h3>{{ theme.name }} <span>{{ theme.version }}</span></h3>
									<div class="scope" ng-show="theme.scope.enabled"></div>
								</div>
							</div>
							<div class="meta">
								<a href="#author" class="author">{{ theme.author.name }}</a><span class="scope"><b>scope:</b> {{ theme.scope.name }}</span>
							</div>
						</div>
						<div class="bottom">
							<a href="#" class="details" ng-click="details(theme.slug)">Details</a>
							<a href="#" class="activate" ng-click="activate(theme.slug)">Activate</a>
						</div>
					</div>
				</li>
				{/literal}
			</ul>

			<div class="loading" ng-show="isLoading"></div>
		</div>
	</div>
</div>
