var app = angular.module('Theme.App', []);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.post['Accept'] = '*/*';
    $httpProvider.defaults.headers.post['modAuth'] = window.config.siteId;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

    $httpProvider.defaults.transformRequest = function(data){
        var p;
        var str = [];
        for(p in data) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
        }
        return str.join("&");
    }
}]);

app.factory('modAjax', ['$http', function ($http) {
    var modAjax = {
        async: function (config) {
            var settings = {
                url: null,
                method: 'POST',
                data: {
                    action: config.action
                },
                cache: true
            };

            angular.extend(settings, config);

            return $http({url: settings.url, method: settings.method, cache: settings.cache, data: settings.data});
        }
    };

    return modAjax;
}])

app.factory('Theme', ['modAjax', function (modAjax) {
	var Theme = {
        all: function () {
            return window.config.scopeThemes;
        },
        save: function (slug) {
            return modAjax.async({
                url: window.config.connectorUrl,
                data: {
                    action: 'mgr/save',
                    slug: slug
                }
            });
        }
    };

	return Theme;
}]);

app.controller('AppController', ['$scope', 'Theme', function ($scope, Theme) {
    $scope.currentTheme = window.config.currentTheme;
	$scope.themes = Theme.all();
    $scope.isLoading = false;

    $scope.activate = function (theme) {
        $scope.isLoading = true;

        Theme.save(theme).success(function (r) {
            // alertify.custom = alertify.extend("custom");
            alertify.success(r.message);
            $scope.statusMsg = r.message;
            if (r.success) {
                $scope.currentTheme = r.object.currentTheme;
                $scope.isLoading = false;
            } else {
                $scope.diff = r.object;
                $scope.isLoading = false;
            }
        });
    }
}]);